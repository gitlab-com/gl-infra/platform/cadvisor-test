def recurse(remaining)
  if remaining.pred > 0
    recurse(remaining.pred)
  else
    remaining
  end
end

def accumulate_fiber_stacks
  fiber = Fiber.new do
    1_000.times { Fiber.yield recurse(200_000) }
    1
  end

  1_000.times { fiber.resume }
end

require 'webrick'

server = WEBrick::HTTPServer.new(Port: 8000)

server.mount_proc '/' do |req, res|
  accumulate_fiber_stacks
  res.body = '# Hi'
end

trap 'INT' do server.shutdown end

server.start
