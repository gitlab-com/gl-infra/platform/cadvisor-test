# cAdvisor testbed

A testbed for investigating https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2024

Uses docker-compose to create containers and monitor them with cAdvisor.

Make sure to [use cgroups v1](https://wiki.archlinux.org/title/cgroups#Enable_cgroup_v1) when testing.

```
docker-compose build
docker-compose up
```

To get cAdvisor with detailed memory stats, do this:

```
git clone -b add-detailed-memory-stats https://github.com/smcgivern/cadvisor
cd cadvisor
docker build . -f deploy/Dockerfile
```

With the image ID in the last step, edit `docker-compose.yml` here to mention that image ID.
